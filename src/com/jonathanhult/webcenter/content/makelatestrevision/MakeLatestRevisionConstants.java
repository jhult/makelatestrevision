package com.jonathanhult.webcenter.content.makelatestrevision;

/*
 * 
 * @author Jonathan Hult
 * 
 */
public class MakeLatestRevisionConstants {
	protected static final String IDC_SERVICE = "IdcService";
	protected static final String TRACE_SECTION = "makelatestrevision";
	protected static final String CHECKOUT_BY_NAME = "CHECKOUT_BY_NAME";
	protected static final String CHECKIN_SEL = "CHECKIN_SEL";
	protected static final String DOC_INFO_SIMPLE = "DOC_INFO_SIMPLE";
	protected static final String DOC_INFO_LATESTRELEASE = "DOC_INFO_LATESTRELEASE";
	protected static final String DOC_INFO = "DOC_INFO";
	protected static final String DID = "dID";
	protected static final String DDOCNAME = "dDocName";
	protected static final String PRIMARY_FILE = "primaryFile";
	protected static final String PRIMARY_FILE_PATH = "primaryFile:path";
	protected static final String ALTERNATE_FILE = "alternateFile";
	protected static final String ALTERNATE_FILE_PATH = "alternateFile:path";
	protected static final String RENDITION_ID = "RenditionId";
	protected static final String WEB = "web";
	protected static final String DOC_SERVICE_HANDLER = "DocServiceHandler";
	protected static final String DDOCID = "dDocID";
	protected static final String DREVLABEL = "dRevLabel";
}