package com.jonathanhult.webcenter.content.makelatestrevision;

import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.ALTERNATE_FILE;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.ALTERNATE_FILE_PATH;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.CHECKIN_SEL;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.CHECKOUT_BY_NAME;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DDOCID;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DDOCNAME;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DID;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DOC_INFO;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DOC_INFO_LATESTRELEASE;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DOC_INFO_SIMPLE;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DOC_SERVICE_HANDLER;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.DREVLABEL;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.PRIMARY_FILE;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.PRIMARY_FILE_PATH;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.RENDITION_ID;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.TRACE_SECTION;
import static com.jonathanhult.webcenter.content.makelatestrevision.MakeLatestRevisionConstants.WEB;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.filestore.IdcFileDescriptor;
import intradoc.server.DocService;
import intradoc.server.DocServiceHandler;
import intradoc.server.ServiceHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*
 * 
 * @author Jonathan Hult
 * 
 */
public class MakeLatestRevisionServiceHandler extends ServiceHandler {
	public void checkinRevisionAsLatest() throws ServiceException {
		traceVerbose("Start checkinAsLatestRevision");
		trace("Binder: " + m_binder.getLocalData().toString());

		// Get dID
		final String dID = m_binder.getLocal(DID);

		// Throw error if dID is missing
		if (dID == null || dID.trim().length() < 1) {
			throw new ServiceException("Could not find required parameter dID.");
		}

		// DataBinder to execute services
		DataBinder serviceBinder = new DataBinder();

		// Get DOC_INFO for dDocName for CHECKOUT, file locations and
		// CHECKIN_SEL service call
		serviceBinder.putLocal(DID, dID);
		executeService(serviceBinder, DOC_INFO_SIMPLE);

		// DOC_INFO ResultSet
		final DataResultSet docInfo = new DataResultSet();
		docInfo.copy(serviceBinder.getResultSet(DOC_INFO));

		// Get dDocName from DOC_INFO service call
		docInfo.first();
		final String dDocName = docInfo.getStringValueByName(DDOCNAME);

		// Reset DataBinder to ensure it is clean for CHECKOUT_BY_NAME
		serviceBinder = new DataBinder();

		// Checkout document
		serviceBinder.putLocal(DDOCNAME, dDocName);
		serviceBinder = executeService(serviceBinder, CHECKOUT_BY_NAME);

		// Reset DataBinder to ensure it is clean for CHECKIN_SEL
		serviceBinder = new DataBinder();

		// Put DOC_INFO information in serviceBinder for CHECKIN_SEL
		serviceBinder.mergeResultSetRowIntoLocalData(docInfo);

		// Retrieve and set primary file information
		final List<String> primaryFileInfo = getFileInfo(serviceBinder, PRIMARY_FILE);

		// Retrieve and set alternate file information
		final List<String> alternateFileInfo = getFileInfo(serviceBinder, WEB);

		// Reset DataBinder to ensure it is clean for DOC_INFO_LATESTRELEASE
		serviceBinder = new DataBinder();

		// Put dDocName in serviceBinder for DOC_INFO_LATESTRELEASE
		serviceBinder.putLocal(DDOCNAME, dDocName);

		// Get DOC_INFO_LATESTRELEASE for latest revision for metadata to use
		// for CHECKIN_SEL
		serviceBinder = executeService(serviceBinder, DOC_INFO_LATESTRELEASE);

		// Merge latest revision metadata into serviceBinder for CHECKIN_SEL
		serviceBinder.mergeResultSetRowIntoLocalData(docInfo);

		// Remove dID to avoid csRevIsNotLatest error
		serviceBinder.removeLocal(DID);

		// Store primary and alternate file information into serviceBinder for
		// CHECKIN_SEL
		serviceBinder.putLocal(PRIMARY_FILE, primaryFileInfo.get(0));
		serviceBinder.putLocal(PRIMARY_FILE_PATH, primaryFileInfo.get(1));
		serviceBinder.putLocal(ALTERNATE_FILE, alternateFileInfo.get(0));
		serviceBinder.putLocal(ALTERNATE_FILE_PATH, alternateFileInfo.get(1));

		final DocServiceHandler docServiceHandler = (DocServiceHandler) ((DocService) m_service).getHandler(DOC_SERVICE_HANDLER);

		try {
			docServiceHandler.addIncrementID(null, 2, DDOCID);
			docServiceHandler.addIncrementID(null, 2, DREVLABEL);
		} catch (final DataException e) {
			trace("Could not get new dDocID and/or dRevLabel; this may cause problems", e);
		}

		// Checkin old revision as latest revision
		executeService(serviceBinder, CHECKIN_SEL);
	}

	/**
	 * This method retrieves the name of either the primary or alternate file
	 * and its location on disk.
	 * 
	 * @param serviceBinder
	 *            DataBinder which contains revision specific metadata needed to
	 *            find file.
	 * @param renditionID
	 *            "primaryFile" for the primary file; "web" for the alternate
	 *            file.
	 * @return A List containing two Strings. The first is the file name. The
	 *         second is the file path.
	 * @throws ServiceException
	 */
	private List<String> getFileInfo(final DataBinder serviceBinder, final String renditionID) throws ServiceException {
		final List<String> fileInfo = new ArrayList<String>(2);
		serviceBinder.putLocal(RENDITION_ID, renditionID);
		try {
			final IdcFileDescriptor idcFileDescriptor = m_service.m_fileStore.createDescriptor(serviceBinder, null, m_service);
			final String filePath = m_service.m_fileStore.getFilesystemPath(idcFileDescriptor, m_service);
			final File file = new File(filePath);
			fileInfo.add(file.getName());
			fileInfo.add(filePath);
		} catch (final DataException e) {
			if (renditionID.equals(PRIMARY_FILE)) {
				throw new ServiceException(e);
			}
		}
		return fileInfo;
	}

	/**
	 * This method executes a service.
	 * 
	 * @param serviceBinder
	 *            DataBinder containing service parameters.
	 * @param serviceName
	 *            String containing name of service to execute.
	 * @throws ServiceException
	 */
	private DataBinder executeService(final DataBinder serviceBinder, final String serviceName) throws ServiceException {
		traceVerbose("Start executeService");

		try {
			trace("Calling service " + serviceName + ": " + serviceBinder.getLocalData().toString());
			// Execute service
			m_service.getRequestImplementor().executeServiceTopLevelSimple(serviceBinder, serviceName, m_service.getUserData());
			trace("Finished calling service");
			return serviceBinder;
		} catch (final DataException e) {
			trace("Something went wrong executing service " + serviceName);
			e.printStackTrace(System.out);
			throw new ServiceException("Something went wrong executing service " + serviceName, e);
		} finally {
			traceVerbose("End executeService");
		}
	}

	/**
	 * This methods traces output to console.
	 * 
	 * @param message
	 *            the String to trace to the console
	 */
	private void trace(final String message) {
		trace(message, null);
	}

	/**
	 * This methods traces output to the UCM console.
	 * 
	 * @param message
	 *            the String to trace to the console
	 */
	private void trace(final String message, final Exception e) {
		Report.trace(TRACE_SECTION, message, e);
	}

	/**
	 * This methods verbose traces output to the UCM console.
	 * 
	 * @param message
	 *            the String to trace to the console
	 */
	private void traceVerbose(final String message) {
		if (Report.m_verbose) {
			trace(message);
		}
	}
}
